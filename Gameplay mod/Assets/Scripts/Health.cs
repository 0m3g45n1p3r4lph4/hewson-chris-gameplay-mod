﻿/**
 *  @Health.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will manage the health of an GameObject and alows you to
 *  take damage as well as display another GameObject when killed.
 */

 //health script used by enemies and player alike.
 //spawns a 'death' prefab upon depletion

using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int maxHealth = 10; //what is the health cap?
	public int health = 10; //what is the current health?
	public GameObject[] deathInstance; //default prefab source. I want to change this to allow more than one object - use array or second prefab, whichever fits best with code
	public Vector2 deathInstanceOffset = new Vector2 (0, 0); //where to but the dead body
    public SpriteRenderer renderSprite;
    public Rigidbody2D rb2D;
    public BoxCollider2D box2D;

	void Start () {
        renderSprite = GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
        box2D = GetComponent<BoxCollider2D>();
		health = maxHealth; //set health on spawn
	}

	public void TakeDamage (int value) { //getting hurt
		Debug.Log ("Take Damage " + value); //write the damage in the log
		health -= value; //decrease health
	
		if (health <= 0) { //when out of health, die;
			OnKill ();
		}
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == "arrow(Clone)" && this.gameObject.name == "Player") { //if the player collides with an arrow
            TakeDamage(collision.gameObject.GetComponent<Attack>().attackValue);
            Destroy(collision.gameObject);
        }
    }

    public void OnKill () { //this is the kill function. why call it OnKill? it sounds weird.
		if (deathInstance[0]) {//if there's a death instance, which there is.
			Vector3 position = gameObject.transform.position; //position the corpse
            //instantiate the corpse
            Instantiate(deathInstance[0], new Vector3 (position.x + deathInstanceOffset.x, position.y + deathInstanceOffset.y, position.z), Quaternion.identity);
		} if (deathInstance[1]) {//a secondary one. nothing should have more than 2, I think
            Vector3 position = gameObject.transform.position; //position the ghost
            //instantiate the ghost
            Instantiate(deathInstance[1], new Vector3(position.x + deathInstanceOffset.x, position.y + deathInstanceOffset.y, position.z), Quaternion.identity);
        }
        if (this.gameObject.tag == "Player") {
            renderSprite.enabled = false; //hide the player
            //rb2D.gravityScale = 0;
            rb2D.simulated = false;
            box2D.enabled = false;
        } else {
            Destroy(this.gameObject); //destroy the original enemy
        }
	}
}
