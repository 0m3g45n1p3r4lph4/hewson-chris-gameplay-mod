﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject[] fortress;

    // Update is called once per frame
    void Update() {
        if (fortress[0] == null && fortress[1] == null) {
            StartCoroutine(VictoryState());
            Time.timeScale-=0.001f;
        }
        if (Time.timeScale <= 0.001) {
            Time.timeScale = 0.001f;
        }
    }

    IEnumerator VictoryState() {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("WinScene");
    }
}
