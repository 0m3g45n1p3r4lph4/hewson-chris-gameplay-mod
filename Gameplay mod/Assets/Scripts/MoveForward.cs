﻿/**
 *  @MoveForward.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will move a GameObject forward based on the direction it
 *  is facing and its speed.
 */

using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

    public float speed = .3f; // Speed to move the GameObject

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
        Physics.IgnoreLayerCollision(9,9);
    }
    void Update() {
        // Set the velocity to the speed multiplies by the direction the GameObject is facting.
        rigidbody2DComponent.velocity = new Vector2(transform.localScale.x, 0) * speed;
    }
}
