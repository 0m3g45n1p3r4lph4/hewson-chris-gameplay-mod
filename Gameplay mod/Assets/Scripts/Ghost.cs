﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 right so first unique script
 a thing to control the ghost moving after the player dies
 nothing complicated
*/

public class Ghost : MonoBehaviour {

    Rigidbody2D ghostRB;//the rigidbody component

    // Start is called before the first frame update
    void Start() {
        ghostRB = GetComponent<Rigidbody2D>(); //getting the rigidbody component
    }

    // Update is called once per frame
    void Update() {
        ghostRB.velocity = new Vector2(0, 1); //moving the ghost
    }
}
