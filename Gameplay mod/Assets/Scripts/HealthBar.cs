﻿/**
 *  @ControlsUI.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script Visualizes the health value on a GameObject with the
 *  Health script attached to it.
 */

 //this is the health bar
 //definitely seperate from health in order to not have all conflicting health bar data from knights

using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    Health healthComponent;
    public Image healthImage;

    void Awake() {
        healthComponent = GetComponent<Health>();
    }
    void Start() {
        healthImage.rectTransform.localScale = new Vector3(1, 1, 1);
    }

    void Update() {
        float percent = healthComponent.health / (float)healthComponent.maxHealth;
        healthImage.rectTransform.localScale = new Vector3(percent, 1, 1);
    }

    /*
     I figured out why the health bar was being stupid
     the player is destroyed before the health bar can update
     at least that seems to be it
     so how to fix??
     */
}
