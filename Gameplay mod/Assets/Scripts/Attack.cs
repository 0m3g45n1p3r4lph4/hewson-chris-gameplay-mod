﻿/**
 *  @Attack.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will allow a GameObject to attack another GameObject if it
 *  has a reference to the Health Script.
 */

 //used as a combat control by knights and the monster
 //gonna see if there's a way to tweak this for archers. if not, I'll need to make a shooting script
 //(or could just add a specific new function in here... duh)

 //I don't like this script. I'm a disaster coder and THIS is a disaster. Everything feels all over the place, when so many of the functions could just be connected easier
 //not to mention this timer...

 //^^saving those notes. this code makes sense, I wasll sick and sleep deprived
 //well not to say I'm not anymore but I'm improving

using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {
	public int attackValue = 1; //how much damage is dealt
	public float attackDelay = 1f; //cooldown between attacks
	public string targetTag; //what are you hurting
	private bool canAttack; //is your cooldown over
    public float punchDelay; //punh cooldown
    private bool canPunch = true;

    public int combatType; //1 for knight/arrow/fist (immediate contact), 2 for archer (projectile), 3 for monster (during specific action)?
    public GameObject projectile; // what does it shoot, if it does?

    // Use this for initialization
    void Start () {
		if (attackValue == 0) //if you don't deal damage...
			canAttack = false; //.. you can't attack.
		else
			StartCoroutine(OnAttack()); //otherwise attack?? at start?????? 
	}

    //stuff happening each frame
    void Update() {
        if (canAttack && combatType == 2) { //gonna handle the shooty dudes in here
            projectile.transform.localScale = this.gameObject.transform.localScale;
            Instantiate(projectile, gameObject.transform.position, gameObject.transform.rotation);
            canAttack = false;
        }
        if (canAttack && combatType == 3 && Input.GetButtonDown("Fire3") && canPunch) {
            projectile.transform.localScale = this.gameObject.transform.localScale;
            Instantiate(projectile, gameObject.transform.position, gameObject.transform.rotation);
            canPunch = false;
            StartCoroutine(OnPunch());
        }
    }

    void OnCollisionStay2D(Collision2D c) { //collisions
		if (c.gameObject.tag == targetTag && canAttack && (combatType == 1 || combatType == 3) && c.gameObject.tag != "Fist") { 
            //if you collide with your target, can attack and would attack...
			TestAttack(c.gameObject); 
            //..then TEST attack??? why is it named like this
            if (gameObject.name == "arrow(Clone)" || gameObject.name == "goat(Clone)") {
                Destroy(this.gameObject);
            }
		}
	}

	void TestAttack(GameObject target) {
        //so it gets the target game object immediately... that's nice
        //facing one way else facing the other
        if (transform.localScale.x == 1) { 
			if (target.transform.position.x > transform.position.x) 
				AttackTarget(target);
		} else { 
			if (target.transform.position.x < transform.position.x)
				AttackTarget(target);
		}
		canAttack = false;
	}

	void AttackTarget(GameObject target) {
        Health healthComponent = target.GetComponent<Health>();
		if (healthComponent)
			healthComponent.TakeDamage(attackValue);
	}

	IEnumerator OnAttack() {
		yield return new WaitForSeconds(attackDelay);
		canAttack = true;
		StartCoroutine(OnAttack());
	}
    /*
     you know things are gonna be big when Chris breaks out the slash-stars
     so this stupid thing is so simple yet somehow makes no sense to me
     so it starts at start, when the object spawns.
     it does its countdown, and then lets the attack happen, then runs itself again, automatically resetting the timer.
     ...where was I confused??
     */

    IEnumerator OnPunch() {
        yield return new WaitForSeconds(punchDelay);
        canPunch = true;
    }
}
